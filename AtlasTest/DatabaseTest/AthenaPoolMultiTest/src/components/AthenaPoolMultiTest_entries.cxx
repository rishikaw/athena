#include "../StoreDump.h"
#include "../PassAllFilter.h"
#include "../PassNoneFilter.h"
#include "../AddTrigMap.h"
#include "../EventSplit.h"
#include "../RunEventMetaWriter.h"
#include "../DummyLumirangeTool.h"

DECLARE_COMPONENT( StoreDump )
DECLARE_COMPONENT( PassAllFilter )
DECLARE_COMPONENT( PassNoneFilter )
DECLARE_COMPONENT( AddTrigMap )
DECLARE_COMPONENT( EventSplit )
DECLARE_COMPONENT( RunEventMetaWriter )
DECLARE_COMPONENT( DummyLumirangeTool )

